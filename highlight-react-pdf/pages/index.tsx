import { Button } from '@chakra-ui/react'
import { useRouter } from 'next/router'
import { Text } from "@chakra-ui/react"


export default function Home() {
  const router = useRouter();


  function handleVisualizar() {
    router.push('/visualizar')
  }


  return (
    <>
      <Button onClick={handleVisualizar}> Visualizar PDF</Button>
      <Text>
        A palavra em destaque é "just"

        É desejado que duas ou mais sejam destacadas, exemplo "just" e "Mechanics"
      </Text>
      <Text>
        Referência:
      </Text>
      <a>https://github.com/wojtekmaj/react-pdf/wiki/Recipes#highlight-text-on-the-page</a>
    </>

  )
}
