import { Box } from "@chakra-ui/react"
import React, { useCallback, useEffect, useState } from "react"
import { Document, Page, pdfjs } from 'react-pdf';

pdfjs.GlobalWorkerOptions.workerSrc = `https://cdnjs.cloudflare.com/ajax/libs/pdf.js/${pdfjs.version}/pdf.worker.js`;

interface Documento {
    binario: String;
}

function highlightPattern(text, pattern) {
    const splitText = text.split(pattern);

    if (splitText.length <= 1) {
        return text;
    }

    const matches = text.match(pattern);

    return splitText.reduce((arr, element, index) => (matches[index] ? [
        ...arr,
        element,
        <mark key={index}>
            {matches[index]}
        </mark>,
    ] : [...arr, element]), []);
}

export function DocumentoView({ binario }: Documento) {
    const [pdf, setPdf] = useState();
    const [numPages, setNumPages] = useState(null);
    const [pageNumber, setPageNumber] = useState(1);
    const [searchText, setSearchText] = useState('just');

    const textRenderer = useCallback(
        (textItem) => {
            return highlightPattern(textItem.str, searchText);
        },
        [searchText]
    );

    function onChange(event) {
        setSearchText(event.target.value);
    }


    useEffect(() => {
        let file = pdfBase64(binario)
        setPdf(file.props.src)

    }, []);


    function onDocumentLoadSuccess({ numPages }) {
        setNumPages(numPages);
        setPageNumber(1);
    }

    function pdfBase64(binario) {
        return <embed src={`data:application/pdf;base64,${binario}`} type="application/pdf" width="100%"></embed>
    }



    return (
        <>
            <Box align="center" mt="20px">
                <Document
                    file={pdf}
                    onLoadSuccess={onDocumentLoadSuccess}
                >
                    {Array.from(
                        new Array(numPages),
                        (el, index) => (
                            <Page
                                width={800}
                                key={`page_${index + 1}`}
                                pageNumber={index + 1}
                                customTextRenderer={textRenderer}
                            />
                        ),
                    )}
                </Document>
            </Box>
        </>

    )
}
